# FAQ

In the following, **My** refers to the self-service GenOuest web site [https://my.genouest.org](https://my.genouest.org).

## I asked for an account but it is not yet created

After registration, you should have received an email with a link to
validate your email.
Once this is done, your account is pending approval by our team.
This operation *may* take a few days, so please be patient.
Once validated you will receive an email with your new, temporary,
password and have access to our services.

## After registration, I cannot login to [**My**](https://my.genouest.org)

Check carefully the password written in the confirmation email...

Also avoid copy/paste as extra characters may be copied (spaces for example).
Try writing the password manually.

After successful login, do not forget to change your password with a
password of your choice (long and complex enough).

## I cannot connect to your services

If your credentials fail on Galaxy, Cesgo, etc, test at first your
credentials on [**My**](https://my.genouest.org).

If it succeeds on [**My**](https://my.genouest.org), it may be a service problem and you can create a support ticket (by writing to support@genouest.org).

If it fail son [**My**](https://my.genouest.org), it means that you forgot your password.

In this case, connect to [**My**](https://my.genouest.org), fill your login identifier and
select the "Lost your password" option.

## I have a "cannot execute xxx" error on cluster

Check that you are not connected to one of our connection frontend (also known as "bastion server") like `genossh.genouest.org`.

Bastions are for connection/access only. To execute a software you need to connect to a compute node, see [slurm usage](usage/cluster.md#slurm) help page.

## Some files from the cluster are not refreshed in [data-access](https://data-access.cesgo.org)

There can be some synchronizing problem between the cluster storage and [data-access](https://data-access.cesgo.org), especially for directories containing many files or subdirectories.

The easiest solution (until we find a better one), is to delete the mount point in data-access:
go to `Parameters`, then `Storage`, at the top right.

Then to recreate the mount point, connect to [My](https://my.genouest.org), and click on the `Details` tab, then click on the `Update` button in the Data-access section.
