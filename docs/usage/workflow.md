# Workflows

## Pre-designed WorkFlows with Nextflow

Nextflow is installed on the cluster, it can be used by sourcing the correct environnement:

```bash
. /local/env/envnextflow-20.04.1.sh
```

nf-core is also preinstalled, allowing you to use standard workflows from [nf-co.re](https://nf-co.re/) using the [nf-core client](https://nf-co.re/tools#listing-pipelines).

Nextflow is preconfigured to use Singularity (when workflows are designed to use containers) and to distribute jobs on the Slurm cluster.

Alternatively, we provide some other pre-designed workflows for RNAseq and small-RNAseq analysis. They can be found in the directory /local/nextflow/ with the following pattern:

```bash
/local/nextflow/type_of_analysis/team_designer/
```

For more convenience, a shell wrapper  is available for each workflow. You just have to copy it into your working directory, for example:

```bash
cp /local/nextflow/rnaseq/nfcore/rnaseq_nfcore.sh ~/workdir/
```

You may need to customize the script to suit your needs (data files, etc..), and run it with SLURM like this:

```bash
sbatch rnaseq_nfcore.sh
```
