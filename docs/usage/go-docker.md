# GO-Docker

GO-Docker is a Slurm-like batch scheduling system. It provides a command-line and a web interface to submit shell scripts in Docker containers. The web interface provides an easy way to submit jobs and get an overview of their status.

A REST interface also eases its integration to other tools for automation.

Basically, one select the CPU/memory requirements, and the container image needed for the computation. Some default images are provided, the genouest one is a clone of the current computation nodes based on CentOS.

It also provides interactive sessions to the tasks, in fact a SSH access the container.

## Access

[https://godocker.genouest.org](https://godocker.genouest.org)

Tutorials

[https://bitbucket.org/osallou/go-docker/wiki/Usertutorial](https://bitbucket.org/osallou/go-docker/wiki/Usertutorial)

Jobs metrics, once job is over can be queried at:

[https://prometheus.genouest.org/graph](https://prometheus.genouest.org/graph)

(see more doc at [https://godocker.atlassian.net/wiki/display/GOD/GODOCKER](https://godocker.atlassian.net/wiki/display/GOD/GODOCKER) and [https://bitbucket.org/osallou/go-docker/wiki/Home](https://bitbucket.org/osallou/go-docker/wiki/Home))

## Screencasts

[![Go-Docker tutorial](https://img.youtube.com/vi/juw_foi-Q0c/0.jpg)](https://www.youtube.com/watch?v=juw_foi-Q0c)

[![Go-Docker CLI tutorial](https://img.youtube.com/vi/3fu2aLocTbI/0.jpg)](https://www.youtube.com/watch?v=3fu2aLocTbI)
