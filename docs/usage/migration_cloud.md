# Cloud migration (December 2020)

A complete overhaul of the Genostack service is planned **from the 14/12/20 to 01/01/21**.  
As the underlying infrastructure will be replaced, **instances, images, volumes and shares will be removed**.  
Several procedures are available to transfer your **images**, **volumes** and **shares**  
Nonetheless, we advise you to **backup** your important data **outside** the cloud.  

## Warnings

**Do not forget to re-add your SSH key in the Genostack interface** (Project -> Compute -> Keypairs)  
**The HTTP access customization service is temporarily offline**.
You can still access the port 80 and 443 of your VM by using "http://myapp-192-168-xxx-xxx.vm.openstack.genouest.org/"  
Please consult [this page](https://www.genouest.org/outils/genostack/getting-started.html#virtual-machine-access-https) for more information.  

## Volumes

You can regenerate your volumes from your backups by going to the *Volumes->Backups* tab, and using the *Restore Backup* action.

## Manila Shares

All your shares content was copied in the */groups/openstack/your_user_id/shares/share_name/* folder.  
You will need to re-create your shares and transfer the data back with the *scp* command.  
Don't forget to re-create the correct access permissions for the share.

## Images

You can re-create your images using the *import.sh* script on the *openstack.genouest.org* frontend. **(genossh.genouest.org will not work)**  

* Download your *openrc* file by going to the *API Access* tab in [Genostack](https://genostack.genouest.org)
* Select **Download Openstack RC File -> OpenStack RC File (Identity API v3)**
* Upload this file to your Genouest home
* Use the *import.sh* script in */groups/openstack/your_user_id/images/* to re-upload your images

```bash
scp yourusernamee-openrc.sh yourusernamee@openstack.genouest.org:
ssh yourusernamee@openstack.genouest.org
. yourusernamee-openrc.sh
bash /groups/openstack/your_user_id/images/import.sh
```

The image will then be available on Genostack.
