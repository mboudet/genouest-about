# Accounts

## Getting an account

To get an account, you should go to [GenOuest account manager](https://my.genouest.org)
and click on *Create an account*.

After form submission, you will receive an email requesting to confirm your
account request and click on the received link.

Your request will be examined and validated by GenOuest. Accounts are presently free for the academic community but things will change in the next months since the operation and maintenance of our infrastructure has a non negligeable cost that we can not support easily.

When your account is approved, you will receive an additional email with connection
information.

At account approval, you are automatically subscribed to our
mailing list (low rate). This list is used to send general information
such as maintenance and service issues.

To unsubscribe or to get more information on our mailing, have a look at [GenOuest mailing list](../policy/mailing_list.md)

## Password reset

If you lose your password, just go to [https://my.genouest.org](https://my.genouest.org), enter your login and click on “RESET”. You will receive the instructions by email.
