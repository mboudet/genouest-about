# CeSGO

[Cesgo](https://cesgo.org) is a set of services hosted at GenOuest around scientific collaboration.

## Collaboration

Based on WordPress, you can collaborate with your team around projects
with documents, news, forums. It can also host your project web site.

## Data access

Based on ownCloud, data-access lets you store, access and share your data remotely.

## Research sharing

Store and publish your research data.

Based on Seek, you can pubish data and metadata and link them with to your experiments.

## Projects

Manage efficiently your projects with a Kanban board (task management).

## Instant

Chat/instant messaging app powered by Rocket.chat.
