# Galaxy

[Galaxy](https://galaxyproject.org/) is a web portal allowing to execute bioinformatics analysis in a user-friendly environment.

[Access our instance](https://galaxy.genouest.org/).
