# About

Our facility provides development, trainings, expertise and resources for bioinformatics,
primarily for academic research.

Provided resources include, but are not limited to, computing, biological
databanks, software and collaboration tools.

## Getting an account

To use most of our resources, you will need an account. Only a few of the web tools we host allow
anonymous usage.

To create an account, see [GenOuest account](usage/create_an_account.md)

## Using our resources

We host computing resources to submit jobs to a HPC cluster, a [Galaxy instance](https://galaxyproject.org/), a private cloud, preinstalled software and [databanks](usage/biomaj.md), and much more...
[See all our services](usage/services.md).

Each account gets a free amount of [data storage](usage/cluster.md#storage). If you have specific data storage requirements, we can discuss together to increase your quotas, however we expect you come with a clear data management plan (DMP).

If you plan to use our resources, please contact us *before* submitting your research project proposal to funding agencies. This will allow us to plan together your requirements and, if needed, the purchase of computing resources.

## Citing and terms of use

To use GenOuest services, you must comply with our [terms of use](policy/terms_of_use.md).

If you publish a research article, we ask you to add a note indicating that you used
our resources. Please see the [Citing GenOuest](policy/citing.md) page

## GDPR and user data collection

All user information stored in GenOuest information system are solely for
GenOuest use and are not shared with anyone else.

Personal information (email, phone, address etc.) is only used by administrators
to contact user in the following cases:

* communication (maintenance, etc.) via a mailing list. User can unsubscribe
  to the mailing list at any time.
* online services can make use of the user email address to send results per user request
* security issues
* service abuse

We do not use cookies to track your usage.
However session cookies are sometimes used to enable you to log in an application.
If you disable cookies in your browser, you won't be able to log in those applications.
Once you logout, all session info are deleted.

## Working at GenOuest

We welcome newcomers, be it a permanent academic, a trainee or a developper
working on one of our wonderful projects. In all cases, each one must
follow the same rules and will be trained to our quality and compute
environment.

* [Human resources information](human_resources.md)
* [Quality management system](quality_management/iso9001.md)
* Like our users, you will need a [GenOuest account](usage/create_an_account.md)
* [Our environment](usage/internal_tools.md)

## What we do

Managing a computing facility also means managing operating systems, networks,
user account and many other things. To do so, we develop internally many
software. And because we use many open source software, almost all our
developments are open source too. They are usually available at
[https://github.com/genouest](https://github.com/genouest).

We also have strong collaborations with other labs/entities such as [IFB](https://www.france-bioinformatique.fr/) (in particular the cluster and cloud actions) and the [Elixir](https://elixir-europe.org/) European infrastructure.

## FAQ

See our [FAQ](faq.md) for common questions/issues.

## Contact

Address:

```text
Plate-forme GenOuest IRISA-INRIA, Campus de Beaulieu 35042 Rennes cedex, France
```

Mail: support@genouest.org

Twitter: [@GenOuest](https://twitter.com/genouest)
