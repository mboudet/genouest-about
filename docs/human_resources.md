# Human resources

## Job opportunities

We are always seeking new talented people to join us!

People with a permanent positions from national research institutes can always join us via mobility, or thanks to annual competition of these national research institutes.

We regularly propose short term (usually 1 to 3 years) contracts on specific development or data analysis projects, often in strong collaboration with other members of national and international projects.

Trainees are also welcome, depending on the ongoing projects.

Don't hesitate to contact us at support@genouest.org for more information.

## Newcomers

GenOuest is hosted at [IRISA](https://www.irisa.fr/), an UMR (mixed research unit), on the Beaulieu campus, in Rennes, France.

GenOuest has strong links with two research teams in IRISA: [Dyliss](https://www-dyliss.irisa.fr/) and [Genscale](https://team.inria.fr/genscale/). You will work in a group of ~50 people working in the various fields of bioinformatics.

We all have different employers (INRIA, Universite Rennes 1, CNRS, etc.), as such, salary, holidays, etc are ruled by your employer.

As a newcomer, you will follow a 0.5 day presentation describing your rights/obligations and local security rules with the local resource department and the security officer.

In addition, you must follow the [IRISA internal regulations](https://intranet.irisa.fr/fr/intranet-fs/irisa/DocAdmin/ReglemementInterieurIRISA.pdf)

At your arrival, you will follow a welcome procedure, tracked with GitLab issues. You will need to complete them all to access all the rights and priviledges of a GenOuest member.
