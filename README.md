# GenOuest documentation

This repository is used to generate automatically the [genouest documentation website](https://help.genouest.org).

Feel free to make any correction or modification by creating a merge request.

To test locally, first install dependencies:

```bash
pip install mkdocs mkdocs-cinder mkdocs-awesome-pages-plugin
```

Then run the dev server:

```bash
mkdocs serve
```

Changes will be taken into account instantly.
